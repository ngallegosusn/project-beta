import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './components/App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

async function loadAppts() {
  const serviceResponse= await fetch('http://localhost:8080/api/appointments');
  const TechResponse= await fetch('http://localhost:8080/api/technicians/');



  if (
    serviceResponse.ok &&
    TechResponse.ok
    ) {
    const apptData = await serviceResponse.json();
    const techData = await TechResponse.json();



    root.render(
      <React.StrictMode>
        <App
        appointments={apptData.appointments}
        technicians={techData.technicians}
        />
      </React.StrictMode>
    )
  }else{
    console.error(serviceResponse)
  }
}
loadAppts()