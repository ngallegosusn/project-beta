import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <div className="container-fluid d-inline-flex flex-row flex-nowrap">
          <NavLink className="navbar-brand" to="/">CarCar</NavLink>
          <ul className="navbar-nav mb-0 mb-lg-0 container-fluid d-flex flex-row">
            <li className="nav-item flex-fill">
              <NavLink className="nav-link text-center" aria-current="page" to="manufacturers" end>Manufacturers</NavLink>
            </li>
            <li className="nav-item flex-fill">
              <NavLink className="nav-link text-center" aria-current="page" to="manufacturers/new">Add Manufacturer</NavLink>
            </li>
            <li className="nav-item flex-fill">
              <NavLink className="nav-link text-center" aria-current="page" to="models" end>Models</NavLink>
            </li>
            <li className="nav-item flex-fill">
              <NavLink className="nav-link text-center" aria-current="page" to="models/new">Add Model</NavLink>
            </li>
            <li className="nav-item flex-fill">
              <NavLink className="nav-link text-center" aria-current="page" to="autos" end>Auto List</NavLink>
            </li>
            <li className="nav-item flex-fill">
              <NavLink className="nav-link text-center" aria-current="page" to="autos/new" end>Add Auto</NavLink>
            </li>
          </ul>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
        </div>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 my-2 mb-lg-0 d-flex flex-row">
            <li className="nav-item flex-fill">
              <NavLink className="nav-link text-center" aria-current="page" to="appointments" end>Scheduled Appointments</NavLink>
            </li>
          <li className="nav-item flex-fill">
              <NavLink className="nav-link text-center" aria-current="page" to="appointments/new">Schedule an appointment</NavLink>
            </li>
            <li className="nav-item flex-fill">
              <NavLink className="nav-link text-center" aria-current="page" to="appointments/history">Service History</NavLink>
            </li>
            <li className="nav-item flex-fill">
              <NavLink className="nav-link text-center" aria-current="page" to="technicians/new">Add a technicican</NavLink>
            </li>
            <li className="nav-item flex-fill">
              <NavLink className="nav-link text-center" aria-current="page" to="technicians" end>Technicians</NavLink>
            </li>
          </ul>
        </div>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0 d-flex flex-row">
            <li className="nav-item flex-fill">
              <NavLink className="nav-link text-center" aria-current="page" to="sales/list">Sales List</NavLink>
            </li>
            <li className="nav-item flex-fill">
              <NavLink className="nav-link text-center" aria-current="page" to="sales/new">New Sale</NavLink>
            </li>
            <li className="nav-item flex-fill">
              <NavLink className="nav-link text-center" aria-current="page" to="customers" end>Customers</NavLink>
            </li>
            <li className="nav-item flex-fill">
              <NavLink className="nav-link text-center" aria-current="page" to="customers/new">New Customer</NavLink>
            </li>
            <li className="nav-item flex-fill">
              <NavLink className="nav-link text-center" aria-current="page" to="salespeople" end>Salespeople</NavLink>
            </li>
            <li className="nav-item flex-fill">
              <NavLink className="nav-link text-center" aria-current="page" to="salespeople/new">New Salesperson</NavLink>
            </li>
            <li className="nav-item flex-fill">
              <NavLink className="nav-link text-center" aria-current="page" to="salespeople/history">Salesperson Records</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
