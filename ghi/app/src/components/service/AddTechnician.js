import { useState} from 'react'

function AddTechnician(){

    const handleSubmit=async(event)=>{
        event.preventDefault()
        const data={}

        data.first_name=fName
        data.last_name=lName
        data.employee_id=empId

    const technicianUrl='http://localhost:8080/api/technicians/';
    const fetchConfig={
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(technicianUrl, fetchConfig);
    if(response.ok){
        const newTechnician=await response.json();

        setFName('');
        setLName('');
        setEmpId('');
    }
    }

    const[fName,setFName]=useState('')
    const handleFNameChange=(event)=>{
        const value=event.target.value
        setFName(value)
    }

    const [lName,setLName]=useState('')
    const handleLNameChange=(event)=>{
        const value=event.target.value
        setLName(value)
    }

    const[empId,setEmpId]=useState('')
    const handleEmpIdChange=(event)=>{
        const value=event.target.value
        setEmpId(value)
    }


    return(
        <div className="row">
  <div className="offset-3 col-6">
    <div className="shadow p-4 mt-4">
      <h1>Create a new Technician</h1>
      <form onSubmit={handleSubmit} id="create-technician-form">
        <div className="form-floating mb-3">
          <input onChange={handleFNameChange} placeholder="First Name" required type="text" name="first name" id="first name" className="form-control" value={fName} />
          <label htmlFor="first_name">First Name</label>
        </div>
        <div className="form-floating mb-3">
          <input onChange={handleLNameChange} placeholder = "last name" required type="text" name="last name" id="last name" className="form-control" value={lName} />
          <label htmlFor="last_name">Last Name</label>
        </div>
        <div className="form-floating mb-3">
            <input onChange={handleEmpIdChange} placeholder="employee id" required type="number" name="employee id" id="employee id" className="form-control" value={empId} />
            <label htmlFor="employee_id">Employee Id</label>
        </div>
        <button className="btn btn-primary">Create</button>
      </form>
    </div>
  </div>
</div>

);
}
export default AddTechnician