import React, {useEffect, useState} from 'react'

function AppointmentForm(){

    const handleSubmit= async(event)=>{
        event.preventDefault()
        const data={}

        data.reason=reason;
        data.date_time=date;
        data.vin=vin;
        data.customer=customer;
        data.technician=tech;
    const appointmentUrl='http://localhost:8080/api/appointments/';
    const fetchConfig={
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(appointmentUrl, fetchConfig);
    if(response.ok){
        const newAppointment= await response.json();

        setReason('');
        setDate('');
        setVin('');
        setVin('');
        setCustomer('');
        setTech('');

    }
    }



    const [date,setDate]=useState('')
    const handleDateChange=(event)=>{
        const value=event.target.value
        setDate(value)
    }

    const[reason,setReason]=useState('')
    const handleReasonChange=(event)=>{
        const value =event.target.value
        setReason(value)
    }


    const[vin,setVin]=useState('')
    const handleVinChange=(event)=>{
        const value = event.target.value
        setVin(value)
    }

    const[customer,setCustomer]=useState('')
    const handleCustChange=(event)=>{
        const value=event.target.value
        setCustomer(value)
    }

    const[tech,setTech]=useState('')
    const handleTechChange=(event)=>{
        const value=event.target.value;
        setTech(value)
    }

    const[technicians,setTechnicians]=useState([])
    const fetchData= async () => {
        const url = 'http://localhost:8080/api/technicians/';
        const response=await fetch(url)
        if (response.ok){
            const data = await response.json();
            setTechnicians(data.technicians)
        }
    }

useEffect(()=>{
    fetchData();
},[])

return(
        <div className="row">
  <div className="offset-3 col-6">
    <div className="shadow p-4 mt-4">
      <h1>Create a new appointment</h1>
      <form onSubmit={handleSubmit} id="create-appointment-form">
        <div className="form-floating mb-3">
          <input onChange={handleDateChange} placeholder="date/time" required type="datetime-local" name="date/time" id="date/time" className="form-control" value={date} />
          <label htmlFor="date/time">Date/time</label>
        </div>
        <div className="form-floating mb-3">
          <input onChange={handleReasonChange} placeholder = "Reason" required type="text" name="reason" id="reason" className="form-control" value={reason} />
          <label htmlFor="reason">Reason</label>
        </div>
        <div className="form-floating mb-3">
            <input onChange={handleVinChange} placeholder="vin" required type="text" name="vin" id="vin"className="form-control" value={vin} />
            <label htmlFor="vin">VIN</label>
        </div>
        <div className="form-floating mb-3">
            <input onChange={handleCustChange} placeholder="customer" required type="text" name="customer" id="customer"className="form-control" value={customer} />
            <label htmlFor="customer">Customer</label>
        </div>
        <div className="mb-3">
          <select onChange={handleTechChange} required id="technician" name="technician" className="form-select" >
            <option value="">Choose a Technician</option>
            {technicians.map(tech => {
                return (
                    <option key={tech.employee_id} value={tech.employee_id}>
                    {tech.first_name} {tech.last_name}
                    </option>
                    );
                })}
          </select>
        </div>
        <button className="btn btn-primary">Create</button>
      </form>
    </div>
  </div>
</div>

);
}
export default AppointmentForm
