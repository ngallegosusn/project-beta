import React, {useEffect, useState,} from "react";

function ApptList(props){
    const [appointments,setAppointments]=useState([]);

    const handleCancel= async(appts)=>{
        try{
            const response=await fetch(`http://localhost:8080${appts.href}cancel/`,{
                method: "PUT",
            });
            if (response.ok){
                setAppointments((appointments)=>
                appointments.map((item)=>
                item.id===appts.id ? {...item, status:"canceled"}: item
                )
                );
            }
        } catch(error){
            console.error("error cancelling appointment", error);
        }
    };

    const handleFinish= async(appts)=>{
        try{
            const response=await fetch(`http://localhost:8080${appts.href}finish/`,{
                method:"PUT",
            });
            if (response.ok){
                setAppointments((appointments)=>
                appointments.map((item)=>
                item.id===appts.id ? { ...item, status:"finished"}: item
                )
                );
            }
        } catch(error){
            console.error("error finishing appointment", error)
        }
    };



useEffect(()=>{
    setAppointments(props.appointments);
    },[props.appointments]);

const filteredAppointments=appointments?.filter(
    (appts)=> appts.status !=="canceled" && appts.status!== "finished"
) ?? [];

return(
    <div className="container my-5">
        <h2>Appointments</h2>
        <table className="table">
            <thead>
                <tr>
                    <th>Date/time</th>
                    <th>Reason</th>
                    <th>Status</th>
                    <th>VIN</th>
                    <th>Customer</th>
                    <th>Technician</th>
                    <th>VIP</th>
                </tr>
            </thead>
                <tbody>
                    {filteredAppointments.map(appts=>{
                        return(
                            <tr className="table-info" key={appts.href} value={appts.href}>
                                <td>{appts.date_time}</td>
                                <td>{appts.reason}</td>
                                <td>{appts.status}</td>
                                <td>{appts.vin}</td>
                                <td>{appts.customer}</td>
                                <td>{appts.technician.first_name}</td>
                                <td>{appts.is_vip ? 'VIP' : 'Regular'}</td>
                                <td>
                                    <button onClick={()=> handleCancel(appts)}>Cancel</button>
                                    <button onClick={()=> handleFinish(appts)}>Finish</button>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
        </table>
    </div>
)
}
export default ApptList;