import React,{useEffect, useState} from 'react'

function ServiceHistory(props){
    const[searchVIN, setSearchVIN]= useState("")
    const[filteredAppointments,setFilteredAppointments]=useState([]);

    const handleSearchSubmit = (event) => {
        event.preventDefault();
        if(searchVIN.trim()===''){
            setFilteredAppointments(props.appointments);
        }else{
            const filtered=props.appointments.filter((appts)=>appts.vin.includes(searchVIN))
            setFilteredAppointments(filtered)
        }
    };
    useEffect(()=>{
        setFilteredAppointments(props.appointments);
    }, [props.appointments])

    return(
        <div className="container my-5">
            <h2>Service History</h2>
            <form onSubmit={handleSearchSubmit}>
                <input type="text" value={searchVIN} onChange={(event)=> setSearchVIN(event.target.value)} placeholder="Enter Vin"/>
                <button type="submit">Search</button>
            </form>
            <table className="table">
                <thead>
                    <tr>
                        <th>Date/time</th>
                        <th>Reason</th>
                        <th>Status</th>
                        <th>VIN</th>
                        <th>Customer</th>
                        <th>Technician</th>
                        <th>VIP</th>
                    </tr>
                </thead>
                    <tbody>
                        {filteredAppointments&&filteredAppointments.map((appts)=>{
                            return(
                                <tr className="table-info" key={appts.href} value={appts.href}>
                                    <td>{appts.date_time}</td>
                                    <td>{appts.reason}</td>
                                    <td>{appts.status}</td>
                                    <td>{appts.vin}</td>
                                    <td>{appts.customer}</td>
                                    <td>{appts.technician.first_name}</td>
                                    <td>{appts.is_vip ? 'VIP' : 'Regular'}</td>
                                </tr>

                            );
                            })}
                    </tbody>
            </table>
        </div>
        );
    }
export default ServiceHistory