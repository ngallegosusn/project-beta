import { useEffect, useState } from "react";

function ListModels() {
    const [models, setModels] = useState()

    const handleDelete = async (event) => {
        event.preventDefault()

        const modelId = event.target.id
        console.log(modelId)
        const deleteUrl = `http://localhost:8100/api/models/${modelId}`
        const fetchConfig  = {
            method: 'delete'
        }
        const deleteRes = await fetch(deleteUrl, fetchConfig)
        if (deleteRes.ok) {
            window.location.reload()
        } else {
            console.error("Problem deleteing model in inventory-api line 14")
        }
    }

    const fetchData = async () => {
        const modelsUrl = "http://localhost:8100/api/models/"
        const modelsRes = await fetch(modelsUrl)
        if (modelsRes.ok) {
            const modelsData = await modelsRes.json()
            setModels(modelsData.models)
        } else {
            console.error("Error fetching manufacturers list from inventory-api line 24")
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="shadow m-5">
            <table className="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th scope="col">Make</th>
                        <th scope="col">Model</th>
                        <th scope="col">Picture</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    {models&&models.map(model => {
                        return (
                        <tr key={model.id} className="align-middle">
                            <td>{model.manufacturer.name}</td>
                            <td>{model.name}</td>
                            <td><img className="" style={{'width': 100 + 'px'}} src={model.picture_url} alt=""/></td>
                            <td onClick={handleDelete} id={model.id} className="btn-md btn-danger text-danger text-center" style={{'width': 70 + 'px'}}>Delete</td>
                        </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}
export default ListModels;
