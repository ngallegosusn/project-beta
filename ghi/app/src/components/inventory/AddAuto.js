import { useEffect, useState } from "react";

function AddAuto() {
    const [makes, setMakes] = useState([])
    const [models, setModels] = useState([])
    const [make, setMake] = useState('')
    const [model, setModel] = useState('')
    const [color, setColor] = useState('')
    const [year, setYear] = useState('')
    const [vin, setVin] = useState('')

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}
        data.model_id = model
        data.color = color
        data.year = year
        data.vin = vin

        const fetchPostConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const autosUrl = 'http://localhost:8100/api/automobiles/'
        const autosRes = await fetch(autosUrl, fetchPostConfig)
        if (autosRes.ok) {
            window.location.reload()
        } else {
            console.error("Problem posting automobile to inventory-api line 29")
        }
    }

    const handleMakeChange = (event) => {
        const value = event.target.value
        setMake(value)
    }

    const handleModelChange = (event) => {
        const value = event.target.value
        setModel(value)
    }

    const handleColorChange = (event) => {
        const value = event.target.value
        setColor(value)
    }

    const handleYearChange = (event) => {
        const value = event.target.value
        setYear(value)
    }

    const handleVinChange = (event) => {
        const value = event.target.value
        setVin(value)
    }

    const fetchData = async () => {
        const modelsUrl = 'http://localhost:8100/api/models/'
        const modelsRes = await fetch(modelsUrl)
        const makesUrl = 'http://localhost:8100/api/manufacturers/'
        const makesRes = await fetch(makesUrl)
        if (modelsRes.ok && makesRes.ok) {
            const makesData = await makesRes.json()
            const modelsData = await modelsRes.json()
            setModels(modelsData.models)
            setMakes(makesData.manufacturers)
        } else {
            console.error("Error getting data from inventory-api lines 64, 66")
        }

    }
    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                    <h1>Create an Automobile</h1>
                    <form onSubmit={handleSubmit}>
                        <div className="mb-3">
                            <select onChange={handleMakeChange} name="make" required id="make" value={make} className="form-select">
                                <option value="">Choose a Make</option>
                                {makes&&makes.map(make => {
                                    return (
                                        <option key={make.id} value={make.id}>
                                            {make.name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleModelChange} name="model" required id="model" value={model} className="form-select">
                                <option value="">Choose a Model</option>
                                {models&&models.filter(model => model.manufacturer.id==make).map(model => {
                                    return (
                                        <option key={model.id} value={model.id}>
                                            {model.name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input name="color" placeholder="Color" onChange={handleColorChange} required type="text" id="color" value={color} className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input name="year" placeholder="Year" onChange={handleYearChange} required type="number" id="year" value={year} className="form-control" />
                            <label htmlFor="year">Year</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input name="vin" placeholder="VIN" onChange={handleVinChange} required type="text" id="vin" value={vin} className="form-control" />
                            <label htmlFor="vin">VIN</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
    )
};

export default AddAuto;

    // const [salespeople, setSalespeople] = useState([])
    // const [customers, setCustomers] = useState([])
    // const [automobiles, setAutomobiles] = useState([])
    // const [salesperson, setSalesperson] = useState('')
    // const [customer, setCustomer] = useState('')
    // const [automobile, setAutomobile] = useState('')
    // const [price, setPrice] = useState('')

    // const handleSubmit = async (event) => {
    //     event.preventDefault()
    //     const data = {}

    //     data.salesperson = salesperson
    //     data.customer = customer
    //     data.automobile = automobile
    //     data.price = price

    //     const fetchPostConfig = {
    //         method: 'post',
    //         body: JSON.stringify(data),
    //         headers: {
    //             'Content-Type': 'application/json',
    //         },
    //     }

    //     const fetchPutConfig = {
    //         method: 'put',
    //         body: JSON.stringify({"sold": true}),
    //         headers: {
    //             'Content-Type': 'application/json',
    //         },
    //     }

    //     const postUrl = 'http://localhost:8090/api/sales/'
    //     const postRes = await fetch(postUrl, fetchPostConfig)
    //     if (postRes.ok) {
    //         const newSale = await postRes.json()
    //         const putUrl = `http://localhost:8100/api/automobiles/${automobile}/`
    //         const putRes = await fetch(putUrl, fetchPutConfig)
    //         if (putRes.ok) {
    //             const updatedAuto = await putRes.json()
    //             log(updatedAuto)
    //         } else {
    //             console.error("Error putting data in inventory-api line XX")
    //         }

    //         console.log(newSale)

    //         setSalesperson('')
    //         setCustomer('')
    //         setAutomobile('')
    //         setPrice('')
    //     } else {
    //         console.error("Error posting data to sales-api line 31")
    //     }
    // }

    // const handleSalespersonChange = (event) => {
    //     const value = event.target.value
    //     setSalesperson(value)
    // }

    // const handleCustomerChange = (event) => {
    //     const value = event.target.value
    //     setCustomer(value)
    // }

    // const handleAutomobileChange = (event) => {
    //     const value = event.target.value
    //     setAutomobile(value)
    // }

    // const handlePriceChange = (event) => {
    //     const value = event.target.value
    //     setPrice(value)
    // }

    // const fetchData = async () => {
    //     const salespeopleUrl = 'http://localhost:8090/api/salespeople/'
    //     const salespeopleRes = await fetch(salespeopleUrl)
    //     const customersUrl = 'http://localhost:8090/api/customers/'
    //     const customersRes = await fetch(customersUrl)
    //     const automobilesUrl = 'http://localhost:8100/api/automobiles/'
    //     const automobilesRes = await fetch(automobilesUrl)
    //     if (salespeopleRes.ok && customersRes.ok && automobilesRes.ok) {
    //         const salespeopleData = await salespeopleRes.json()
    //         const customersData = await customersRes.json()
    //         const automobilesData = await automobilesRes.json()
    //         setSalespeople(salespeopleData.salespeople)
    //         setCustomers(customersData.customers)
    //         setAutomobiles(automobilesData.autos)
    //     } else {
    //         console.error("Error getting data from sales-api lines 13-17")
    //     }

    // }
    // useEffect(() => {
    //     fetchData()
    // }, [])

    // return (
    //     <div className="offset-3 col-6">
    //                 <div className="shadow p-4 mt-4">
    //                 <h1>Create a Sale Record</h1>
    //                 <form onSubmit={handleSubmit} id="create-sale-form">
    //                     <div className="mb-3">
    //                         <select onChange={handleSalespersonChange} name="salesperson" required id="salesperson" value={salesperson} className="form-select">
    //                             <option value="">Choose a Salesperson</option>
    //                             {salespeople&&salespeople.map(Make => {
    //                                 return (
    //                                     <option key={Make.id} value={Make.id}>
    //                                         {Make.first_name} {Make.last_name}
    //                                     </option>
    //                                 )
    //                             })}
    //                         </select>
    //                     </div>
    //                     <div className="mb-3">
    //                         <select onChange={handleCustomerChange} name="customer" required id="customer" value={customer} className="form-select">
    //                             <option value="">Choose a Customer</option>
    //                             {customers&&customers.map(cust => {
    //                                 return (
    //                                     <option key={cust.id} value={cust.id}>
    //                                         {cust.first_name} {cust.last_name}
    //                                     </option>
    //                                 )
    //                             })}
    //                         </select>
    //                     </div>
    //                     <div className="mb-3">
    //                         <select onChange={handleAutomobileChange} name="automobile" required id="autombile" value={automobile} className="form-select">
    //                             <option value="">Choose an Auto</option>
    //                             {automobiles&&automobiles.filter(auto => auto.sold===false).map(auto => {
    //                                 return (
    //                                     <option key={auto.vin} value={auto.vin}>
    //                                         {auto.vin}
    //                                     </option>
    //                                 )
    //                             })}
    //                         </select>
    //                     </div>
    //                     <div className="form-floating mb-3">
    //                         <input name="price" placeholder="Price" onChange={handlePriceChange} required type="number" id="price" value={price} className="form-control" />
    //                         <label htmlFor="price">Price</label>
    //                     </div>
    //                     <div className="mb-3">
    //                     </div>
    //                     <button className="btn btn-primary">Create</button>
    //                 </form>
    //             </div>
    //         </div>
    // )
    // function RecordSale() {
    //     const [salespeople, setSalespeople] = useState([])
    //     const [customers, setCustomers] = useState([])
    //     const [automobiles, setAutomobiles] = useState([])
    //     const [salesperson, setSalesperson] = useState('')
    //     const [customer, setCustomer] = useState('')
    //     const [automobile, setAutomobile] = useState('')
    //     const [price, setPrice] = useState('')

    //     const handleSubmit = async (event) => {
    //         event.preventDefault()
    //         const data = {}

    //         data.salesperson = salesperson
    //         data.customer = customer
    //         data.automobile = automobile
    //         data.price = price

    //         const fetchPostConfig = {
    //             method: 'post',
    //             body: JSON.stringify(data),
    //             headers: {
    //                 'Content-Type': 'application/json',
    //             },
    //         }

    //         const fetchPutConfig = {
    //             method: 'put',
    //             body: JSON.stringify({"sold": true}),
    //             headers: {
    //                 'Content-Type': 'application/json',
    //             },
    //         }

    //         const postUrl = 'http://localhost:8090/api/sales/'
    //         const postRes = await fetch(postUrl, fetchPostConfig)
    //         if (postRes.ok) {
    //             const newSale = await postRes.json()
    //             const putUrl = `http://localhost:8100/api/automobiles/${automobile}/`
    //             const putRes = await fetch(putUrl, fetchPutConfig)
    //             if (putRes.ok) {
    //                 const updatedAuto = await putRes.json()
    //                 log(updatedAuto)
    //             } else {
    //                 console.error("Error putting data in inventory-api line XX")
    //             }

    //             console.log(newSale)

    //             setSalesperson('')
    //             setCustomer('')
    //             setAutomobile('')
    //             setPrice('')
    //         } else {
    //             console.error("Error posting data to sales-api line 31")
    //         }
    //     }

    //     const handleSalespersonChange = (event) => {
    //         const value = event.target.value
    //         setSalesperson(value)
    //     }

    //     const handleCustomerChange = (event) => {
    //         const value = event.target.value
    //         setCustomer(value)
    //     }

    //     const handleAutomobileChange = (event) => {
    //         const value = event.target.value
    //         setAutomobile(value)
    //     }

    //     const handlePriceChange = (event) => {
    //         const value = event.target.value
    //         setPrice(value)
    //     }

    //     const fetchData = async () => {
    //         const salespeopleUrl = 'http://localhost:8090/api/salespeople/'
    //         const salespeopleRes = await fetch(salespeopleUrl)
    //         const customersUrl = 'http://localhost:8090/api/customers/'
    //         const customersRes = await fetch(customersUrl)
    //         const automobilesUrl = 'http://localhost:8100/api/automobiles/'
    //         const automobilesRes = await fetch(automobilesUrl)
    //         if (salespeopleRes.ok && customersRes.ok && automobilesRes.ok) {
    //             const salespeopleData = await salespeopleRes.json()
    //             const customersData = await customersRes.json()
    //             const automobilesData = await automobilesRes.json()
    //             setSalespeople(salespeopleData.salespeople)
    //             setCustomers(customersData.customers)
    //             setAutomobiles(automobilesData.autos)
    //         } else {
    //             console.error("Error getting data from sales-api lines 13-17")
    //         }

    //     }
    //     useEffect(() => {
    //         fetchData()
    //     }, [])

    //     return (
    //         <div className="offset-3 col-6">
    //                     <div className="shadow p-4 mt-4">
    //                     <h1>Create a Sale Record</h1>
    //                     <form onSubmit={handleSubmit} id="create-sale-form">
    //                         <div className="mb-3">
    //                             <select onChange={handleSalespersonChange} name="salesperson" required id="salesperson" value={salesperson} className="form-select">
    //                                 <option value="">Choose a Salesperson</option>
    //                                 {salespeople&&salespeople.map(Make => {
    //                                     return (
    //                                         <option key={Make.id} value={Make.id}>
    //                                             {Make.first_name} {Make.last_name}
    //                                         </option>
    //                                     )
    //                                 })}
    //                             </select>
    //                         </div>
    //                         <div className="mb-3">
    //                             <select onChange={handleCustomerChange} name="customer" required id="customer" value={customer} className="form-select">
    //                                 <option value="">Choose a Customer</option>
    //                                 {customers&&customers.map(cust => {
    //                                     return (
    //                                         <option key={cust.id} value={cust.id}>
    //                                             {cust.first_name} {cust.last_name}
    //                                         </option>
    //                                     )
    //                                 })}
    //                             </select>
    //                         </div>
    //                         <div className="mb-3">
    //                             <select onChange={handleAutomobileChange} name="automobile" required id="autombile" value={automobile} className="form-select">
    //                                 <option value="">Choose an Auto</option>
    //                                 {automobiles&&automobiles.filter(auto => auto.sold===false).map(auto => {
    //                                     return (
    //                                         <option key={auto.vin} value={auto.vin}>
    //                                             {auto.vin}
    //                                         </option>
    //                                     )
    //                                 })}
    //                             </select>
    //                         </div>
    //                         <div className="form-floating mb-3">
    //                             <input name="price" placeholder="Price" onChange={handlePriceChange} required type="number" id="price" value={price} className="form-control" />
    //                             <label htmlFor="price">Price</label>
    //                         </div>
    //                         <div className="mb-3">
    //                         </div>
    //                         <button className="btn btn-primary">Create</button>
    //                     </form>
    //                 </div>
    //             </div>
    //     )
    // };
