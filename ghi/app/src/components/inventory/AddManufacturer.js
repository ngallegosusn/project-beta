import { useState } from "react";

function AddManufacturer() {
    const [name, setName] = useState('')

    const handleSubmit = async (event) => {
        event.preventDefault()

        const postUrl = 'http://localhost:8100/api/manufacturers/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify({'name': name}),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const postRes = await fetch(postUrl, fetchConfig)
        if (postRes.ok) {
            window.location.reload()
        } else {
            console.error("Problem posting manufacturer to inventory-api line 17")
        }
    }

    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
    }

    return (
        <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                    <h1>Create a Manufacturer</h1>
                    <form onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input name="name" placeholder="Name" onChange={handleNameChange} required type="text" id="name" value={name} className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
    )
}
export default AddManufacturer;
