import { useEffect, useState } from "react"

function ListAutos() {
    const [automobiles , setAutomobiles] = useState()

    const handleDelete = async (event) => {
        event.preventDefault()

        const vin = event.target.id
        const autosUrl = `http://localhost:8100/api/automobiles/${vin}/`
        const fetchConfig = {
            method: 'delete'
        }
        const deleteResponse = await fetch(autosUrl, fetchConfig)
        if (deleteResponse.ok) {
            window.location.reload()
        } else {
            console.error("Problem deleting autos in inventory api line 14")
        }
    }

    const fetchData = async () => {
        const autosUrl = "http://localhost:8100/api/automobiles/"
        const autosReponse = await fetch(autosUrl)
        if (autosReponse.ok) {
            const autosData = await autosReponse.json()
            setAutomobiles(autosData.autos)
        } else {
            console.error("Error fetching autos from inventory-api line 24")
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
            <div className="shadow m-5">
                <table className="table table-striped table-bordered table-hover">
                    <thead className="">
                        <tr>
                            <th scope="col">VIN</th>
                            <th scope="col">Color</th>
                            <th scope="col">Year</th>
                            <th scope="col">Make</th>
                            <th scope="col">Model</th>
                            <th scope="col">Sold</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        {automobiles&&automobiles.map(auto => {
                            return (
                            <tr key={auto.vin} className="align-middle">
                                <td>{auto.vin}</td>
                                <td>{auto.color}</td>
                                <td>{auto.year}</td>
                                <td>{auto.model.manufacturer.name}</td>
                                <td>{auto.model.name}</td>
                                <td style={{'width': 75 + 'px'}}>{auto.sold ? "Sold" : "Unsold"}</td>
                                <td onClick={handleDelete} id={auto.vin} className="btn-md btn-danger text-danger">Delete</td>
                            </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
    )
};

export default ListAutos;
