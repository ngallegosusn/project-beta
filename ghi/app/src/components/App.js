import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from '../MainPage';
import Nav from '../Nav';
import ApptList from './service/ListAppointments';
import AppointmentForm from './service/AppointmentForm';
import TechList from './service/listTechnician';
import AddTechnician from './service/AddTechnician';
import ServiceHistory from './service/ServiceHistory';
import SalesHome from './sales/SalesHome';
import ListSales from './sales/ListSales';
import RecordSale from './sales/RecordSale';
import ListSalespeople from './sales/ListSalespeople';
import AddSalesperson from './sales/AddSalesperson';
import SalespersonHistory from './sales/SalespersonHistory';
import ListCustomers from './sales/ListCustomers';
import AddCustomer from './sales/AddCustomer';
import ListAutos from './inventory/ListAutos';
import AddAuto from './inventory/AddAuto';
import ListManufacturers from './inventory/ListManufacturers';
import AddManufacturer from './inventory/AddManufacturer';
import ListModels from './inventory/ListModels';
import AddModel from './inventory/AddModel';



function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="appointments" >
            <Route path= ""  element = {<ApptList appointments ={props.appointments}/>}/>
            <Route path="new" element={<AppointmentForm/>}/>
            <Route path ='history' element={<ServiceHistory appointments={props.appointments}/>} />
          </Route>
          <Route path="/technicians">
            <Route path = "" element={<TechList technicians={props.technicians}/>}/>
            <Route path="new" element={<AddTechnician/>}/>
          </Route>
          <Route path="manufacturers">
            <Route path="" element={<ListManufacturers />} />
            <Route path="new" element={<AddManufacturer />} />
          </Route>
          <Route path="models">
            <Route path="" element={<ListModels />} />
            <Route path="new" element={<AddModel />} />
          </Route>
          <Route path="autos">
            <Route path="" element={<ListAutos />} />
            <Route path="new" element={<AddAuto />} />
          </Route>
          <Route path="sales">
            <Route path="" element={<SalesHome />}/>
            <Route path="list" element={<ListSales />}/>
            <Route path="new" element={<RecordSale />} />
          </Route>
          <Route path="salespeople">
            <Route path="" element={<ListSalespeople />}/>
            <Route path="new" element={<AddSalesperson />}/>
            <Route path="history" element={<SalespersonHistory />}/>
          </Route>
          <Route path="customers">
            <Route path="" element={<ListCustomers />}/>
            <Route path="new" element={<AddCustomer />}/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
