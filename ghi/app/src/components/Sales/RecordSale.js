import { useEffect, useState } from "react";

function RecordSale() {
    const [salespeople, setSalespeople] = useState([])
    const [customers, setCustomers] = useState([])
    const [automobiles, setAutomobiles] = useState([])
    const [salesperson, setSalesperson] = useState('')
    const [customer, setCustomer] = useState('')
    const [automobile, setAutomobile] = useState('')
    const [price, setPrice] = useState('')

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}
        data.salesperson = salesperson
        data.customer = customer
        data.automobile = automobile
        data.price = price

        const fetchPostConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const fetchPutConfig = {
            method: 'put',
            body: JSON.stringify({"sold": true}),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const postUrl = 'http://localhost:8090/api/sales/'
        const postRes = await fetch(postUrl, fetchPostConfig)
        if (postRes.ok) {
            const putUrl = `http://localhost:8100/api/automobiles/${automobile}/`
            const putRes = await fetch(putUrl, fetchPutConfig)
            if (!putRes.ok) {
                console.error("Error putting data in inventory-api line 41")
            }
            window.location.reload()
        } else {
            console.error("Error posting sale to sales-api line 38")
        }
    }

    const handleSalespersonChange = (event) => {
        const value = event.target.value
        setSalesperson(value)
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value
        setCustomer(value)
    }

    const handleAutomobileChange = (event) => {
        const value = event.target.value
        setAutomobile(value)
    }

    const handlePriceChange = (event) => {
        const value = event.target.value
        setPrice(value)
    }

    const fetchData = async () => {
        const salespeopleUrl = 'http://localhost:8090/api/salespeople/'
        const salespeopleRes = await fetch(salespeopleUrl)
        const customersUrl = 'http://localhost:8090/api/customers/'
        const customersRes = await fetch(customersUrl)
        const automobilesUrl = 'http://localhost:8100/api/automobiles/'
        const automobilesRes = await fetch(automobilesUrl)
        if (salespeopleRes.ok && customersRes.ok && automobilesRes.ok) {
            const salespeopleData = await salespeopleRes.json()
            const customersData = await customersRes.json()
            const automobilesData = await automobilesRes.json()
            setSalespeople(salespeopleData.salespeople)
            setCustomers(customersData.customers)
            setAutomobiles(automobilesData.autos)
        } else {
            console.error("Error getting data from api(s) lines 73-77")
        }

    }
    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                    <h1>Create a Sale Record</h1>
                    <form onSubmit={handleSubmit}>
                        <div className="mb-3">
                            <select onChange={handleSalespersonChange} name="salesperson" required id="salesperson" value={salesperson} className="form-select">
                                <option value="">Choose a Salesperson</option>
                                {salespeople&&salespeople.map(SPs => {
                                    return (
                                        <option key={SPs.id} value={SPs.id}>
                                            {SPs.first_name} {SPs.last_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleCustomerChange} name="customer" required id="customer" value={customer} className="form-select">
                                <option value="">Choose a Customer</option>
                                {customers&&customers.map(cust => {
                                    return (
                                        <option key={cust.id} value={cust.id}>
                                            {cust.first_name} {cust.last_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleAutomobileChange} name="automobile" required id="autombile" value={automobile} className="form-select">
                                <option value="">Choose an Auto</option>
                                {automobiles&&automobiles.filter(auto => auto.sold===false).map(auto => {
                                    return (
                                        <option key={auto.vin} value={auto.vin}>
                                            {auto.vin}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input name="price" placeholder="Price" onChange={handlePriceChange} required type="number" id="price" value={price} className="form-control" />
                            <label htmlFor="price">Price</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
    )
};

export default RecordSale;
