import { useState } from "react";

function AddSalesperson() {
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [employeeId, setEmployeeId] = useState('')

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}
        data.first_name = firstName
        data.last_name = lastName
        data.employee_id = employeeId

        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const postUrl = 'http://localhost:8090/api/salespeople/'
        const postRes = await fetch(postUrl, fetchConfig)
        if (postRes.ok) {
            window.location.reload()
        } else {
            console.error("Error posting data to sales-api line 25")
        }
    }

    const handleFirstNameChange = (event) => {
        const value = event.target.value
        setFirstName(value)
    }

    const handleLastNameChange = (event) => {
        const value = event.target.value
        setLastName(value)
    }

    const handleEmployeeIdChange = (event) => {
        const value = event.target.value
        setEmployeeId(value)
    }

    return (
        <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                    <h1>Create a Salesperson</h1>
                    <form onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input name="firstName" placeholder="First Name" onChange={handleFirstNameChange} required type="text" id="firstName" value={firstName} className="form-control" />
                            <label htmlFor="firstName">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input name="lastName" placeholder="Last Name" onChange={handleLastNameChange} required type="text" id="lastName" value={lastName} className="form-control" />
                            <label htmlFor="lastName">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input name="employeeId" placeholder="Employee ID" onChange={handleEmployeeIdChange} required type="text" id="employeeId" value={employeeId} className="form-control" />
                            <label htmlFor="employeeId">Employee ID</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
    )
};

export default AddSalesperson;
