import { useEffect, useState } from "react"

function ListSalespeople() {
    const [salespeople, setSalespeople] = useState([])

    const handleDelete = async (event) => {
        event.preventDefault()

        const id = event.target.id
        const salespeopleUrl = `http://localhost:8090/api/salespeople/${id}/`
        const fetchConfig = {
            method: 'delete'
        }
        const deleteResponse = await fetch(salespeopleUrl, fetchConfig)
        if (deleteResponse.ok) {
            window.location.reload()
        } else {
            console.error("Problem deleting salesperson in sales-api line 14")
        }
    }

    const fetchData = async () => {
        const salespeopleUrl = "http://localhost:8090/api/salespeople/"
        const salespeopleReponse = await fetch(salespeopleUrl)
        if (salespeopleReponse.ok) {
            const salespeopleData = await salespeopleReponse.json()
            setSalespeople(salespeopleData.salespeople)
        } else {
            console.error("Error fetching salespeople list from sales-api line 24")
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="shadow m-5">
            <table className="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th scope="col">Salesperson First name</th>
                        <th scope="col">Salesperson Last Name</th>
                        <th scope="col">Employee ID</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    {salespeople&&salespeople.map(SPs => {
                        return (
                        <tr key={SPs.id} className="align-middle">
                            <td>{SPs.first_name}</td>
                            <td>{SPs.last_name}</td>
                            <td>{SPs.employee_id}</td>
                            <td onClick={handleDelete} id={SPs.id} className="btn-md btn-danger text-danger">Delete</td>
                        </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
};

export default ListSalespeople;
