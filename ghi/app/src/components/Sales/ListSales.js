import { useEffect, useState } from "react"

function ListSales() {
    const [sales, setSales] = useState([])

    const handleDelete = async (event) => {
        event.preventDefault()

        const id = event.target.id
        const salesUrl = `http://localhost:8090/api/sales/${id}/`
        const fetchConfig = {
            method: 'delete'
        }
        const deleteResponse = await fetch(salesUrl, fetchConfig)
        if (deleteResponse.ok) {
            window.location.reload()
        } else {
            console.error("Problem deleting sale in sales-api line 14")
        }
    }

    const fetchData = async () => {
        const salesUrl = "http://localhost:8090/api/sales/"
        const salesReponse = await fetch(salesUrl)
        if (salesReponse.ok) {
            const salesData = await salesReponse.json()
            setSales(salesData.sales)
        } else {
            console.error("Error fetching sales list from sales-api line 24")
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="shadow m-5">
            <table className="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th scope="col">Saleaperson Employee ID</th>
                        <th scope="col">Salesperson Name</th>
                        <th scope="col">Customer</th>
                        <th scope="col">VIN</th>
                        <th scope="col">Price</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map(sale => {
                        return (
                        <tr key={sale.id} className="align-middle">
                            <td>{sale.salesperson.employee_id}</td>
                            <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                            <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>${sale.price}</td>
                            <td onClick={handleDelete} id={sale.id} className="btn-md btn-danger text-danger">Delete</td>
                        </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
};

export default ListSales;
