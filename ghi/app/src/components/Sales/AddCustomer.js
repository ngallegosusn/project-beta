import { useState } from "react";

function AddCustomer() {
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [address, setAddress] = useState('')
    const [phoneNumber, setPhoneNumber] = useState('')

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}
        data.first_name = firstName
        data.last_name = lastName
        data.address = address
        data.phone_number = phoneNumber

        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const postUrl = 'http://localhost:8090/api/customers/'
        const postRes = await fetch(postUrl, fetchConfig)
        if (postRes.ok) {
            window.location.reload()
        } else {
            console.error("Error posting data to sales-api line 27")
        }
    }

    const handleFirstNameChange = (event) => {
        const value = event.target.value
        setFirstName(value)
    }

    const handleLastNameChange = (event) => {
        const value = event.target.value
        setLastName(value)
    }

    const handleAddressChange = (event) => {
        const value = event.target.value
        setAddress(value)
    }

    const handlePhoneNumberChange = (event) => {
        const value = event.target.value
        setPhoneNumber(value)
    }

    return (
        <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                    <h1>Create a Customer</h1>
                    <form onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input name="firstName" placeholder="First Name" onChange={handleFirstNameChange} required type="text" id="firstName" value={firstName} className="form-control" />
                            <label htmlFor="firstName">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input name="lastName" placeholder="Last Name" onChange={handleLastNameChange} required type="text" id="lastName" value={lastName} className="form-control" />
                            <label htmlFor="lastName">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input name="address" placeholder="Address" onChange={handleAddressChange} required type="text" id="address" value={address} className="form-control" />
                            <label htmlFor="address">Address</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input name="phoneNumber" placeholder="phoneNumber (number only)" onChange={handlePhoneNumberChange} required type="number" id="phoneNumber" value={phoneNumber} className="form-control" />
                            <label htmlFor="phoneNumber">Phone Number (number only)</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
    )
};

export default AddCustomer;
