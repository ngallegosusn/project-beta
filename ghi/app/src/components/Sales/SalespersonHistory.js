import { useEffect, useState } from "react";

function SalespersonHistory() {
    const [salespeople, setSalespeople] = useState([])
    const [sales, setSales] = useState([])
    const [salesperson, setSalesperson] = useState('')

    const handleDropdownChange = (event) => {
        const value = event.target.value
        setSalesperson(value)
    }

    const fetchData = async () => {
        const salespeopleUrl = 'http://localhost:8090/api/salespeople/'
        const salespeopleRes = await fetch(salespeopleUrl)
        const salesUrl = 'http://localhost:8090/api/sales/'
        const salesRes = await fetch(salesUrl)
        if (salespeopleRes.ok && salesRes.ok) {
            const salespeopleData = await salespeopleRes.json()
            setSalespeople(salespeopleData.salespeople)
            const salesData = await salesRes.json()
            setSales(salesData.sales)
        } else {
            console.error("Error getting data from sales-api lines 15, 17")
        }

    }
    useEffect(() => {
        fetchData()
    }, [])


    return (
        <>
        <div className="my-3 card">
            <select onChange={handleDropdownChange} name="automobile" required id="autombile" value={salesperson} className="form-select">
                <option value="">Choose a Salesperson</option>
                {salespeople&&salespeople.map(SP => {
                    return (
                        <option key={SP.id} value={SP.id}>
                            {SP.first_name} {SP.last_name}
                        </option>
                    )
                })}
            </select>
        </div>
        <div className="shadow m-5">
            <table className="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th scope="col">Salesperson Name</th>
                        <th scope="col">Customer</th>
                        <th scope="col">VIN</th>
                        <th scope="col">Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales&&sales.filter(sale => sale.salesperson.id == salesperson).map(sale => {
                        return (
                            <tr key={sale.id}>
                                <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                                <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                                <td>{sale.automobile.vin}</td>
                                <td>${sale.price}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
        </>
    )
};

export default SalespersonHistory;
