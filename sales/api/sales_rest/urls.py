from django.urls import path
from .views import (
    show_salespeople_list,
    delete_salesperson,
    show_customers_list,
    delete_customer,
    show_sales_list,
    delete_sale
)


urlpatterns = [
    path(
        'salespeople/',
        show_salespeople_list,
        name="show_salespeople_list",
    ),
    path(
        'salespeople/<int:pk>/',
        delete_salesperson,
        name="delete_salesperson",
    ),
    path(
        'customers/',
        show_customers_list,
        name="show_customer_list",
    ),
    path(
        'customers/<int:pk>/',
        delete_customer,
        name="delete_customer",
    ),
    path(
        'sales/',
        show_sales_list,
        name="show_sales_list",
    ),
    path(
        'sales/<int:pk>/',
        delete_sale,
        name="delete_sale",
    ),
]
