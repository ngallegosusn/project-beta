# from django.shortcuts import render
from django.http import JsonResponse
import json
from django.views.decorators.http import require_http_methods
from .encoders import (
    SalespersonEncoder,
    CustomerEncoder,
    SaleEncoder,
)
from .models import (
    Salesperson,
    Customer,
    Sale,
    AutomobileVO,
)


# Create your views here.
@require_http_methods({"GET", "POST"})
def show_salespeople_list(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False
            )
        except Exception:
            return JsonResponse(
                {"message": "Error posting salesperson to database"},
                status=400
            )


@require_http_methods(["DELETE"])
def delete_salesperson(request, pk):
    try:
        deleted, _ = Salesperson.objects.get(id=pk).delete()
    except Exception:
        return JsonResponse({"message": "Invalid salesperson id"}, status=404)
    return JsonResponse(
        {"deleted": deleted > 0}
    )


@require_http_methods({"GET", "POST"})
def show_customers_list(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False
            )
        except Exception:
            return JsonResponse(
                {"messages": "Error posting customer to database"},
                status=400
            )


@require_http_methods(["DELETE"])
def delete_customer(request, pk):
    try:
        deleted, _ = Customer.objects.get(id=pk).delete()
    except Customer.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid customer id"},
            status=404
        )
    return JsonResponse(
        {"deleted": deleted > 0}
    )


@require_http_methods({"GET", "POST"})
def show_sales_list(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            salesperson = Salesperson.objects.get(id=content['salesperson'])
            content['salesperson'] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Salesperson ID"},
                status=404
            )
        try:
            customer = Customer.objects.get(id=content['customer'])
            content['customer'] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Customer ID"},
                status=404
            )
        try:
            automobile = AutomobileVO.objects.get(vin=content['automobile'])
            content['automobile'] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Automobile VIN"},
                status=404
            )
        try:
            sale = Sale.objects.create(**content)
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False
            )
        except Exception:
            return JsonResponse(
                {"message": "Error posting sale to database"},
                status=400
            )


@require_http_methods(["DELETE"])
def delete_sale(request, pk):
    try:
        deleted, _ = Sale.objects.get(id=pk).delete()
    except Exception:
        return JsonResponse({"message": "Invalid sale id"}, status=404)
    return JsonResponse(
        {"deleted": deleted > 0}
    )
