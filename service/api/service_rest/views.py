from .models import Technician, Appointment, AutomobileVO
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from .encoders import (
    TechListEncoder,
    AppointmentEncoder
)


@require_http_methods(["PUT"])
def cancel_appt(request, pk):
    appointment = Appointment.objects.get(id=pk)
    appointment.status = 'canceled'
    appointment.save()
    return JsonResponse(
        appointment,
        encoder=AppointmentEncoder,
        safe=False,
        )


@require_http_methods(["PUT"])
def finish_appt(request, pk):
    appointment = Appointment.objects.get(id=pk)
    appointment.status = "finished"
    appointment.save()
    return JsonResponse(
        appointment,
        encoder=AppointmentEncoder,
        safe=False,
    )


@require_http_methods(["GET", "POST"])
def list_appts(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder
        )
    else:
        content = json.loads(request.body)
        vin = content['vin']
        is_vip = AutomobileVO.objects.filter(vin=vin).exists()
        content['is_vip'] = is_vip
        try:
            technician_id = content['technician']
            technician = Technician.objects.get(
                employee_id=technician_id
            )
            content['technician'] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Technician Id"},
                status=404,
            )
        try:
            appointment = Appointment.objects.create(**content)
        except Exception:
            return JsonResponse(
                {"message": "Failed posting appointment"},
                status=400
            )
    return JsonResponse(
        appointment,
        encoder=AppointmentEncoder,
        safe=False
    )


@require_http_methods(["GET", "PUT", "DELETE"])
def appt_detail(request, pk):
    try:
        appointment = Appointment.objects.get(id=pk)
    except Appointment.DoesNotExist:
        return JsonResponse(
            {"message": "invalid appointment"},
            status=404,
        )
    if request.method == "GET":
        return JsonResponse(
            {"appointment": appointment},
            encoder=AppointmentEncoder
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        Appointment.objects.filter(id=pk).update(**content)
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )
    else:
        deleted, _ = Appointment.objects.get(id=pk).delete()
        return JsonResponse(
            {"Deleted": deleted > 0}
        )


@require_http_methods(["GET", "POST"])
def list_techs(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechListEncoder
            )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
    return JsonResponse(
        technician,
        encoder=TechListEncoder,
        safe=False
    )


@require_http_methods(["GET", "PUT", "DELETE"])
def tech_detail(request, pk):
    try:
        technician = Technician.objects.get(id=pk)
    except Exception:
        return JsonResponse(
            {"message": "Invalid employee"},
            status=404
        )
    if request.method == "GET":
        return JsonResponse(
            {"technicican": technician},
            encoder=TechListEncoder
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        Technician.objects.filter(id=pk).update(**content)
        technician = Technician.objects.get(id=pk)
        return JsonResponse(
            technician,
            encoder=TechListEncoder,
            safe=False,
        )
    else:
        deleted, _ = Technician.objects.get(id=pk).delete()
        return JsonResponse(
            {"Deleted": deleted > 0}
        )
