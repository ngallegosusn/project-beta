from django.urls import path
from .views import (
    list_techs,
    tech_detail,
    list_appts,
    appt_detail,
    finish_appt,
    cancel_appt
)

urlpatterns = [
    path("technicians/", list_techs, name="list_techs"),
    path("technicians/<int:pk>/", tech_detail, name="tech_detail"),
    path("appointments/", list_appts, name="list_appts"),
    path("appointments/<int:pk>/", appt_detail, name="appt_detail"),
    path("appointments/<int:pk>/finish/", finish_appt, name="finish_appt"),
    path("appointments/<int:pk>/cancel/", cancel_appt, name="cancel_appt"),
]
